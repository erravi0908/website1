import React from 'react';
import { Routes, Route, Link } from "react-router-dom";
import Home from '../components/Home';
import AboutUs from '../components/Navbar/AboutUs';

function WebsiteCustomRoutes(props) {
    return (
        <>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="about" element={<AboutUs/>} />
            </Routes>
        </>
    );
}

export default WebsiteCustomRoutes;