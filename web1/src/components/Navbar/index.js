import React from 'react';
import { Link } from "react-router-dom";
import './style.css'
function WebsiteNavBar(props) {
    return (

        <>
        
            <nav style={{backgroundColor:'pink'}}>
                <ul style={{ display: 'flex',listStyle:'none', justifyContent: 'flex-end' }}>
                    <li>Register</li>

                    <li>Home</li>

                    <li>Login</li>

                    <li>Services</li>
                    <li>About Us</li>

                </ul>
            </nav>


        </>
    );
}

export default WebsiteNavBar;
/*

Container properties
1.display:flex
2. flex-direction:row
3. justify-content:  (it is across main axisie.e horizontally) center
4. align-items:to align item vertically i.e cross axis : center,flex-start
5. flex-wrap: wrap|no-wrap
6. algin-Content: center.
*/

/* flex-items properties
1. order: order of apperance
2. flex-grow:1
3. flex-shrink: 1
4. flex:none ;This is the shorthand for flex-grow, flex-shrink and flex-basis combined
5. align-self: auto | flex-start | flex-end | center | baseline | stretch;


*/

/*
 <div style={{ display: 'flex',padding:'1rem',backgroundColor:'pink'}}>
            <div style={{ backgroundColor: 'red',  }}>
                <Link to="/">Login</Link>
            </div>
            <div style={{ backgroundColor: 'red',  }}>
                <Link to="/">Register</Link>
            </div>
            <div style={{ backgroundColor: 'red', }}>
                <Link to="/">Services </Link>
            </div>
            <div style={{ backgroundColor: 'red', }}>
                <Link to="/">Customer Service </Link>
            </div>
            <div style={{ backgroundColor: 'red', }}>
                <Link to="/">About Us</Link>
            </div>
           
        </div>*/