import logo from './logo.svg';
import './App.css';
import WebsiteCustomRoutes from './routes';

function App() {
  return (
    <>
        <WebsiteCustomRoutes/>
    </>
  );
}

export default App;
